﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class dodatnieDatyZgloszenia : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "dataZgloszenia",
                table: "ZgloszoneUslugi",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dataZgloszenia",
                table: "ZgloszoneUslugi");
        }
    }
}
