﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Uslugi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NazwaUslugi = table.Column<string>(nullable: true),
                    Kategoria = table.Column<string>(nullable: true),
                    Wojewodztwo = table.Column<string>(nullable: true),
                    Powiat = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true),
                    ZdjecieUrl = table.Column<string>(nullable: true),
                    MiniaturkaUrl = table.Column<string>(nullable: true),
                    IdUzytkownika = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Uslugi", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Uslugi");
        }
    }
}
