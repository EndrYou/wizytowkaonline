﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class zmianaWUsludzeIdUzytkownikaZIntNaString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdUzytkownika",
                table: "Uslugi",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdUzytkownika",
                table: "Uslugi",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
