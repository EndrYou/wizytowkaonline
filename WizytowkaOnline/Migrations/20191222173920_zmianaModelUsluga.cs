﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class zmianaModelUsluga : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Wojewodztwo",
                table: "Uslugi",
                newName: "stronaInternetowa");

            migrationBuilder.AddColumn<string>(
                name: "Imie",
                table: "Uslugi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Miejscowosc",
                table: "Uslugi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nazwisko",
                table: "Uslugi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "Uslugi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "nrTelefonu",
                table: "Uslugi",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Imie",
                table: "Uslugi");

            migrationBuilder.DropColumn(
                name: "Miejscowosc",
                table: "Uslugi");

            migrationBuilder.DropColumn(
                name: "Nazwisko",
                table: "Uslugi");

            migrationBuilder.DropColumn(
                name: "email",
                table: "Uslugi");

            migrationBuilder.DropColumn(
                name: "nrTelefonu",
                table: "Uslugi");

            migrationBuilder.RenameColumn(
                name: "stronaInternetowa",
                table: "Uslugi",
                newName: "Wojewodztwo");
        }
    }
}
