﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class dataDodaniaUslugi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MiniaturkaUrl",
                table: "Uslugi",
                newName: "DataDodaniaUslugi");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DataDodaniaUslugi",
                table: "Uslugi",
                newName: "MiniaturkaUrl");
        }
    }
}
