﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class zmianaDateTimeNaString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "DataDodaniaOpini",
                table: "Opinie",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DataDodaniaOpini",
                table: "Opinie",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
