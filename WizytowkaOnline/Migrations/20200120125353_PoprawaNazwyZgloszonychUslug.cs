﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WizytowkaOnline.Migrations
{
    public partial class PoprawaNazwyZgloszonychUslug : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ZglszoneUslugi",
                table: "ZglszoneUslugi");

            migrationBuilder.RenameTable(
                name: "ZglszoneUslugi",
                newName: "ZgloszoneUslugi");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ZgloszoneUslugi",
                table: "ZgloszoneUslugi",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ZgloszoneUslugi",
                table: "ZgloszoneUslugi");

            migrationBuilder.RenameTable(
                name: "ZgloszoneUslugi",
                newName: "ZglszoneUslugi");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ZglszoneUslugi",
                table: "ZglszoneUslugi",
                column: "Id");
        }
    }
}
