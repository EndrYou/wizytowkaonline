﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WizytowkaOnline.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WizytowkaOnline.Controllers
{
    public class UslugaController : Controller
    {
        private readonly IUslugaRepository _uslugaRepository;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IOpiniaRepository _opiniaRepository;
        private IHostingEnvironment _env;

        public UslugaController(IUslugaRepository uslugaRepository, UserManager<IdentityUser> userManager, IOpiniaRepository opiniaRepository, IHostingEnvironment env)
        {
            _uslugaRepository = uslugaRepository;
            _userManager = userManager;
            _opiniaRepository = opiniaRepository;
            _env = env;
        }

        // GET: /<controller>/
        [Authorize]
        public IActionResult Index()
        {
            var userId = _userManager.GetUserId(User);

            return View(_uslugaRepository.PobierzWszystkieUslugiUzytkownika(userId));
        }

        public IActionResult Details(int id)
        {
            dynamic UslugiOpinie = new ExpandoObject();

            UslugiOpinie.Uslugi = _uslugaRepository.PobierzUslugePoId(id);
            UslugiOpinie.Opinie = _opiniaRepository.PobierzOpiniePoId(id);

            if (UslugiOpinie.Uslugi == null)
                return NotFound();

            return View(UslugiOpinie);
        }

        [Authorize]
        public IActionResult Create(string FileName)
        {
            var userId = _userManager.GetUserId(User);

            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/images/" + FileName;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult Create(Usluga usluga)
        {
            if(ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);
                DateTime dataDodaniaUslugi = DateTime.Now;

                usluga.DataDodaniaUslugi = dataDodaniaUslugi.ToString("d");

                usluga.IdUzytkownika =userId;
                _uslugaRepository.DodajUsluge(usluga);

                return RedirectToAction("Index");
            }
            return View(usluga);
        }

        public IActionResult Edit(int id, string FileName)
        {
            var usluga = _uslugaRepository.PobierzUslugePoId(id);

            if (usluga == null)
                return NotFound();

            if (!string.IsNullOrEmpty(FileName))
                ViewBag.ImgPath = "/images/" + FileName;
            else
                ViewBag.ImgPath = usluga.ZdjecieUrl;

            return View(usluga);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Usluga usluga)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);

                usluga.IdUzytkownika = userId;
                _uslugaRepository.EdytujUsluge(usluga);

                return RedirectToAction("Index");
            }
            return View(usluga);
        }

        public IActionResult Delete(int id)
        {
            var usluga = _uslugaRepository.PobierzUslugePoId(id);

            if (usluga == null)
                return NotFound();

            return View(usluga);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id, string ZdjecieUrl)
        {
            var usluga = _uslugaRepository.PobierzUslugePoId(id);
            _uslugaRepository.UsunUsluge(usluga);

            if(ZdjecieUrl != null)
            {
                var webRoot = _env.WebRootPath;
                var filePath = Path.Combine(webRoot.ToString() + ZdjecieUrl);

                System.IO.File.Delete(filePath);
            }

            return RedirectToAction("Index");
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile(IFormCollection form)
        {
            var webRoot = _env.WebRootPath;

            var filePath = Path.Combine(webRoot.ToString() + "\\images\\" + form.Files[0].FileName);

            if(form.Files[0].FileName.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await form.Files[0].CopyToAsync(stream);
                }
            }

            if (Convert.ToString(form["Id"]) == string.Empty || Convert.ToString(form["Id"]) == "0")
            {
                return RedirectToAction(nameof(Create), new { FileName = Convert.ToString(form.Files[0].FileName) }); 
            }

            return RedirectToAction(nameof(Edit), new { FileName = Convert.ToString(form.Files[0].FileName) , id = Convert.ToString(form["Id"]) });
        }

        public IActionResult Zglos(int id)
        {
            var usluga = _uslugaRepository.PobierzUslugePoId(id);

            if (usluga == null)
                return NotFound();

            return View(usluga);
        }

        [HttpPost, ActionName("Zglos")]
        [ValidateAntiForgeryToken]
        public IActionResult ZgloszenieConfirm(int id)
        {
            var usluga = _uslugaRepository.PobierzUslugePoId(id);
            var zgloszenie = new ZgloszonaUsluga();

            zgloszenie.IdUslugi = usluga.Id;


            DateTime dataZgloszenia = DateTime.Now;
            zgloszenie.dataZgloszenia = dataZgloszenia.ToString();

            _uslugaRepository.ZglosUsluge(zgloszenie);

            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles="admin")]
        public IActionResult AdminZgoszoneUslugi()
        {
            return View(_uslugaRepository.PobierzWszystkieZgloszoneUslugi());
        }
    }
}
