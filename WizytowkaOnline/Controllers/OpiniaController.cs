﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WizytowkaOnline.Models;

namespace WizytowkaOnline.Controllers
{
    public class OpiniaController : Controller
    {
        private readonly IOpiniaRepository _opiniaRepository;
        private readonly IUslugaRepository _uslugaRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public OpiniaController(IOpiniaRepository opiniaRepository, IUslugaRepository uslugaRepository, UserManager<IdentityUser> userManager)
        {
            _opiniaRepository = opiniaRepository;
            _uslugaRepository = uslugaRepository;
            _userManager = userManager;
        }

        public IActionResult Index(int id)
        {
            ViewData["IdUslugi"] = id;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(Opinia opinia)
        {
            if (ModelState.IsValid)
            {
                DateTime dataDodaniaOpini = DateTime.Now;
                opinia.DataDodaniaOpini = dataDodaniaOpini.ToString();

                if(opinia.NazwaUzytkownika == opinia.Email)
                {
                    var user = await _userManager.GetUserAsync(User);
                    opinia.Email = user.Email;
                }

                _opiniaRepository.DodajOpinie(opinia);
                return RedirectToAction("Szczegoly", "Home", new { id = opinia.IdUslugi});
            }
            return View(opinia);
        }

        public IActionResult UsunOpinie(int id)
        {
            var opinia = _opiniaRepository.PobierzJednaOpinie(id);

            return View(opinia);
        }

        [HttpPost, ActionName("UsunOpinie")]
        [ValidateAntiForgeryToken]
        public IActionResult UsunOpinieConfirmed(int id)
        {
            var opinia = _opiniaRepository.PobierzJednaOpinie(id);
            _opiniaRepository.UsunOpinie(opinia);

            return RedirectToAction("Szczegoly", "Home", new { id = opinia.IdUslugi });
        }
    }
}