﻿using Microsoft.AspNetCore.Mvc;
using System.Dynamic;
using WizytowkaOnline.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WizytowkaOnline.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUslugaRepository _uslugaRepository;
        private readonly IOpiniaRepository _opiniaRepository;

        public HomeController(IUslugaRepository uslugaRepository, IOpiniaRepository opiniaRepository)
        {
            _uslugaRepository = uslugaRepository;
            _opiniaRepository = opiniaRepository;
        }

        public IActionResult Index()
        {
            ViewBag.Tytul = "Przeglad uslug";

            var uslugi = _uslugaRepository.PobierzWszystkieUslugi();

            return View(uslugi);
        }

        [HttpPost]
        public IActionResult Index(string fraza, string powiat, string kategoria)
        {
            ViewBag.Tytul = "Przeglad uslug";

            var uslugi = _uslugaRepository.PobierzWszystkieUslugi();

            if (!string.IsNullOrEmpty(fraza) || !string.IsNullOrEmpty(powiat) || !string.IsNullOrEmpty(kategoria))
            {
                return View(_uslugaRepository.FiltrujWszystkieUslugiUzytkownika(fraza, powiat, kategoria));
            }

            return View(uslugi);
        }

        public IActionResult Szczegoly(int id)
        {
            dynamic UslugiOpinie = new ExpandoObject();

            UslugiOpinie.Uslugi = _uslugaRepository.PobierzUslugePoId(id);
            UslugiOpinie.Opinie = _opiniaRepository.PobierzOpiniePoId(id);

            if (UslugiOpinie.Uslugi == null)
                return NotFound();

            return View(UslugiOpinie);
        }
    }
}
