﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace WizytowkaOnline.Models
{
    public interface IOpiniaRepository
    {
        void DodajOpinie(Opinia opinia);
        void UsunOpinie(Opinia opinia);
        Opinia PobierzJednaOpinie(int id);

        IEnumerable<Opinia> PobierzOpiniePoId(int id);
    }
}
