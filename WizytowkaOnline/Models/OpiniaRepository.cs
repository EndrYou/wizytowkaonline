﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WizytowkaOnline.Models
{
    public class OpiniaRepository : IOpiniaRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly UserManager<IdentityUser> _userManager;


        public OpiniaRepository(AppDbContext appDbContext, UserManager<IdentityUser> userManager)
        {
            _appDbContext = appDbContext;
            _userManager = userManager;
        }

        public void DodajOpinie(Opinia opinia)
        {
            _appDbContext.Opinie.Add(opinia);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Opinia> PobierzOpiniePoId(int id)
        {
            return _appDbContext.Opinie.Where(o => o.IdUslugi == id).OrderByDescending(op => op.Id);
        }

        public Opinia PobierzJednaOpinie(int id)
        {
            return _appDbContext.Opinie.FirstOrDefault(o => o.Id == id);
        }

        public void UsunOpinie(Opinia opinia)
        {
            _appDbContext.Opinie.Remove(opinia);
            _appDbContext.SaveChanges();
        }
    }
}
