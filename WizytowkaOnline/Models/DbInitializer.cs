﻿using System.Linq;

namespace WizytowkaOnline.Models
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            if (!context.Uslugi.Any())
            {
                //context.AddRange(
                //    new Usluga { NazwaUslugi = "Moje uslugi remontowe", Kategoria = "Remont", Miejscowosc = "Opolskie", Powiat = "Kedzierzyn-kozle", Opis = "remontujemy domy wszystko", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika = "1" },
                //    new Usluga { NazwaUslugi = "Moje uslugi budowlane", Kategoria = "Budowa domu", Miejscowosc = "Opolskie", Powiat = "Opole", Opis = "budujemy domy i nie tylko", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika = "2 "},
                //    new Usluga { NazwaUslugi = "Moje uslugi elektryczne", Kategoria = "Elektryk", Miejscowosc = "Opolskie", Powiat = "Glubczyce", Opis = "instalacje elektryczne wszystko prad", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika =" 2 "},
                //    new Usluga { NazwaUslugi = "Moje uslugi hydrauliczne", Kategoria = "Hydraulik", Miejscowosc = "Opolskie", Powiat = "Kluczbork", Opis = "rury woda to moja przygoda", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika = "1 "},
                //    new Usluga { NazwaUslugi = "Moje uslugi ogrodnicze", Kategoria = "Ogrod", Miejscowosc = "Opolskie", Powiat = "Brzeg", Opis = "koszenie trawnikow, obcanie drzewek i krzewow", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika =" 2" },
                //    new Usluga { NazwaUslugi = "Moje uslugi stolarskie", Kategoria = "Stolarstwo", Miejscowosc = "Opolskie", Powiat = "Krapkowice", Opis = "Meble drzwi i wszystko co chcesz", ZdjecieUrl = "/images/zdjecie.jpg", MiniaturkaUrl = "/images/zdjecie.jpg", IdUzytkownika = "1 "}
                //);
            }
            context.SaveChanges();
        }

    }
}
