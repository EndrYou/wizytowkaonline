﻿namespace WizytowkaOnline.Models
{
    public class Usluga
    {
        public int Id { get; set; }
        public string NazwaUslugi { get; set; }
        public string Kategoria { get; set; }
        public string Miejscowosc { get; set; }
        public string Powiat { get; set; }
        public string Opis { get; set; }
        public string ZdjecieUrl { get; set; }
        public string DataDodaniaUslugi { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string nrTelefonu { get; set; }
        public string email { get; set; }
        public string stronaInternetowa { get; set; }
        public string IdUzytkownika { get; set; }
    }
}
