﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace WizytowkaOnline.Models
{
    public class Opinia
    {
        [BindNever]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana")]
        [StringLength(100, ErrorMessage = "Nazwa użytkownika jest za długa")]
        public string NazwaUzytkownika { get; set; }

        [Required(ErrorMessage = "Email jest wymagany")]
        [StringLength(100, ErrorMessage = "Email jest za długi")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Wiadomość jest wymagana")]
        [StringLength(5000, ErrorMessage = "Wiadomość jest za długa")]
        public string Wiadomosc { get; set; }

        public string DataDodaniaOpini { get; set; }

        public int IdUslugi { get; set; }
    }
}
