﻿using System.Collections.Generic;
using System.Linq;

namespace WizytowkaOnline.Models
{
    public class UslugaRepository : IUslugaRepository
    {
        private readonly AppDbContext _appDbContext;

        public UslugaRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public Usluga PobierzUslugePoId(int id)
        {
            return _appDbContext.Uslugi.FirstOrDefault(u => u.Id == id);
        }
        
        public IEnumerable<Usluga> PobierzWszystkieUslugi()
        {
            return _appDbContext.Uslugi;
        }

        public IEnumerable<Usluga> PobierzWszystkieUslugiUzytkownika(string idUser)
        {
            return _appDbContext.Uslugi.Where(u => u.IdUzytkownika == idUser);
        }

        public IEnumerable<Usluga> FiltrujWszystkieUslugiUzytkownika(string fraza, string powiat, string kategoria)
        {
            if(fraza != null && powiat != null && kategoria == null)
                return _appDbContext.Uslugi.Where(u => u.NazwaUslugi.Contains(fraza) ||
                                    u.Opis.Contains(fraza) ||
                                    u.Miejscowosc.Contains(fraza) ||
                                    u.Kategoria.Contains(fraza) &&
                                    u.Powiat.Contains(powiat));

            if (fraza != null && kategoria != null && powiat == null)
                return _appDbContext.Uslugi.Where(u => u.NazwaUslugi.Contains(fraza) ||
                                    u.Opis.Contains(fraza) ||
                                    u.Powiat.Contains(fraza) ||
                                    u.Miejscowosc.Contains(fraza) &&
                                    u.Kategoria.Contains(kategoria));

            if (powiat != null && kategoria != null && fraza == null)
                return _appDbContext.Uslugi.Where(u => u.Powiat.Contains(powiat) &&
                                    u.Kategoria.Contains(kategoria));

            if (fraza != null && kategoria != null && powiat != null)
                return _appDbContext.Uslugi.Where(u => u.NazwaUslugi.Contains(fraza) ||
                                    u.Opis.Contains(fraza) ||
                                    u.Miejscowosc.Contains(fraza) &&
                                    u.Powiat.Contains(powiat) &&
                                    u.Kategoria.Contains(kategoria));

            return _appDbContext.Uslugi.Where(u => u.NazwaUslugi.Contains(fraza) ||
                                    u.Opis.Contains(fraza) ||
                                    u.Kategoria.Contains(fraza) ||
                                    u.Miejscowosc.Contains(fraza) ||
                                    u.Powiat.Contains(fraza) ||
                                    u.Powiat.Contains(powiat) ||
                                    u.Kategoria.Contains(kategoria));
        }

        public void DodajUsluge(Usluga usluga)
        {
            _appDbContext.Uslugi.Add(usluga);
            _appDbContext.SaveChanges();
        }

        public void EdytujUsluge(Usluga usluga)
        {
            _appDbContext.Uslugi.Update(usluga);
            _appDbContext.SaveChanges();
        }

        public void UsunUsluge(Usluga usluga)
        {
            _appDbContext.Uslugi.Remove(usluga);
            _appDbContext.SaveChanges();
        }

        public void ZglosUsluge(ZgloszonaUsluga usluga)
        {
            _appDbContext.ZgloszoneUslugi.Add(usluga);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<ZgloszonaUsluga> PobierzWszystkieZgloszoneUslugi()
        {
            return _appDbContext.ZgloszoneUslugi.OrderByDescending(u => u.Id);
        }
    }
}
