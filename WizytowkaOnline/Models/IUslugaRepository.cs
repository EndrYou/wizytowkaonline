﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WizytowkaOnline.Models
{
    public interface IUslugaRepository
    {
        IEnumerable<Usluga> PobierzWszystkieUslugi();
        Usluga PobierzUslugePoId(int id);
        IEnumerable<Usluga> PobierzWszystkieUslugiUzytkownika(string idUser);
        IEnumerable<Usluga> FiltrujWszystkieUslugiUzytkownika(string fraza, string powiat, string kategoria);
        IEnumerable<ZgloszonaUsluga> PobierzWszystkieZgloszoneUslugi();

        void DodajUsluge(Usluga usluga);
        void EdytujUsluge(Usluga usluga);
        void UsunUsluge(Usluga usluga);
        void ZglosUsluge(ZgloszonaUsluga usluga);

    }
}
