﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace WizytowkaOnline.Models
{
    public class AppDbContext : IdentityDbContext<IdentityUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base (options)
        {
        }

        public DbSet<Usluga> Uslugi { get; set; }
        public DbSet<Opinia> Opinie { get; set; }
        public DbSet<ZgloszonaUsluga> ZgloszoneUslugi { get; set; }
    }
}
