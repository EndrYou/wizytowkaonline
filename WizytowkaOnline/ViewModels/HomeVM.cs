﻿using System.Collections.Generic;
using WizytowkaOnline.Models;

namespace WizytowkaOnline.ViewModels
{
    public class HomeVM
    {
        public string Tytul { get; set; }
        public List<Usluga> Uslugi { get; set; }
    }
}
