﻿using System.ComponentModel.DataAnnotations;

namespace WizytowkaOnline.ViewModels
{
    public class RegisterVM
    {
        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana")]
        [Display(Name = "Nazwa użytkownika")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane")]
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane")]
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Wpisałeś błędne hasło")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Adres email jest wymagany")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
    }
}
